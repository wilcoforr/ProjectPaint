//Project Paint
//Main Activity


package csce305.projectpaint;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.view.View.OnClickListener;

import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import android.app.AlertDialog;
import android.app.Dialog;

import android.provider.MediaStore;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.pes.androidmaterialcolorpickerdialog.ColorPicker;

public class MainActivity extends AppCompatActivity implements OnClickListener  {

    private CanvasView canvasView;

    private ImageButton currentPaintSelected;
    private ImageButton brushButton;
    private ImageButton eraseButton;
    private ImageButton newCanvasButton;
    private ImageButton savePaintingButton;

    private float smallBrushSize;
    private float mediumBrushSize;
    private float largeBrushSize;


    public ColorPicker colorPicker;
    public int brushSizeForSeekBar = 20;


    //onCreate - set up canvas and its elements (color picker, icons, etc)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //hide title
        //requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_main);
        //LinearLayout paintLayout = (LinearLayout)findViewById(R.id.paint_colors);

        colorPicker = new ColorPicker(this);


        canvasView = (CanvasView)findViewById(R.id.drawing);

        //currentPaintSelected = (ImageButton)paintLayout.getChildAt(1);

        //currentPaintSelected.setImageDrawable(getResources().getDrawable(R.drawable.paint_pressed, getTheme()));
        //currentPaintSelected.setImageDrawable(getResources().getDrawable(R.drawable.paint_pressed));

        smallBrushSize = getResources().getInteger(R.integer.small_size);
        mediumBrushSize = getResources().getInteger(R.integer.medium_size);
        largeBrushSize = getResources().getInteger(R.integer.large_size);

        canvasView.setBrushSize(smallBrushSize);

        brushButton = (ImageButton) findViewById(R.id.brushButton);
        brushButton.setOnClickListener(this);

        eraseButton = (ImageButton)findViewById(R.id.eraseButton);
        eraseButton.setOnClickListener(this);

        newCanvasButton = (ImageButton)findViewById(R.id.newCanvasButton);
        newCanvasButton.setOnClickListener(this);

        savePaintingButton = (ImageButton)findViewById(R.id.saveButton);
        savePaintingButton.setOnClickListener(this);
    }


    //paint Clicked
    public void paintClicked(View view){
        if (view != currentPaintSelected) {
            ImageButton imgView = (ImageButton)view;
            String color = view.getTag().toString();
            canvasView.setColor(color);

            //imgView.setImageDrawable(getResources().getDrawable(R.drawable.paint_pressed));
            //currentPaintSelected.setImageDrawable(getResources().getDrawable(R.drawable.paint));
            imgView.setImageDrawable(getResources().getDrawable(R.drawable.paint_pressed, getTheme()));
            currentPaintSelected.setImageDrawable(getResources().getDrawable(R.drawable.paint, getTheme()));
            currentPaintSelected = (ImageButton) view;
        }
    }


    //
    @Override
    public void onClick(View view){
        if (view.getId() == R.id.brushButton){
            setBrushSize();
        }
        else if (view.getId() == R.id.eraseButton){
            setEraserSize();
        }
        else if (view.getId() == R.id.newCanvasButton){
            displayNewCanvasDialog();
        }
        else if (view.getId() == R.id.saveButton) {
            displaySavePaintingDialog();
        }
    }


    //set the size of the brush - either round or square
    //by using a "seek bar" (slider)
    private void setBrushSize() {
        final Dialog brushDialog = new Dialog(this);
        brushDialog.setTitle("Brush size:");
        brushDialog.setContentView(R.layout.brush_chooser);

        ImageButton roundBrush = (ImageButton)brushDialog.findViewById(R.id.round_brush);
        roundBrush.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {
//                canvasView.setBrushSize(smallBrushSize);
//                canvasView.setPreviousBrushSize(smallBrushSize);
                canvasView.setBrushToRoundBrush();
                canvasView.setErase(false);
                brushDialog.dismiss();
            }
        });
        ImageButton squareBrush = (ImageButton)brushDialog.findViewById(R.id.square_brush);
        squareBrush.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {
                canvasView.setBrushToSquareBrush();
                canvasView.setErase(false);
                brushDialog.dismiss();
            }
        });

        SeekBar seekBar = (SeekBar) brushDialog.findViewById(R.id.brushSizeSeekBar);
        //seekBar.incrementProgressBy(1);
        //seekBar.setMax(100);

        seekBar.setProgress(brushSizeForSeekBar);
        TextView seekBarValue = (TextView) brushDialog.findViewById(R.id.brushSizeSeekBarValue);
        seekBarValue.setText(String.valueOf(brushSizeForSeekBar));

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView seekBarValue = (TextView) brushDialog.findViewById(R.id.brushSizeSeekBarValue);
                seekBarValue.setText(String.valueOf(progress));
                brushSizeForSeekBar = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(canvasView != null) {
                    canvasView.setBrushSize(seekBar.getProgress());
                }
            }
        });
        canvasView.setErase(false);


        brushDialog.show();
    }


    
    //set eraser size
    private void setEraserSize() {
        final Dialog brushDialog = new Dialog(this);
        brushDialog.setTitle("Eraser size:");
        brushDialog.setContentView(R.layout.brush_chooser);

        brushDialog.show();

        SeekBar seekBar = (SeekBar) brushDialog.findViewById(R.id.brushSizeSeekBar);
        //seekBar.incrementProgressBy(1);
        //seekBar.setMax(100);

        seekBar.setProgress(brushSizeForSeekBar);
        TextView seekBarValue = (TextView) brushDialog.findViewById(R.id.brushSizeSeekBarValue);
        seekBarValue.setText(String.valueOf(brushSizeForSeekBar));
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView seekBarValue = (TextView) brushDialog.findViewById(R.id.brushSizeSeekBarValue);
                seekBarValue.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(canvasView != null)
                {
                    canvasView.setBrushSize(seekBar.getProgress());

                }

            }
        });
        canvasView.setErase(true);
    }


    //Save the image to the device's gallery
    private void displaySavePaintingDialog() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},23 );
        ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, 23);

        //set write and read permissions
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,  Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                //todo? or empty.
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},23 );
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, 23);
            }
        }

        AlertDialog.Builder saveDialog = new AlertDialog.Builder(this);
        saveDialog.setTitle("Save painting");
        saveDialog.setMessage("Save painting to the Gallery?");
        saveDialog.setPositiveButton("Save painting", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which){
                canvasView.setDrawingCacheEnabled(true);

                //generate semi random name
                String imgSaved = MediaStore.Images.Media.insertImage(
                        getContentResolver(), canvasView.getDrawingCache(),
                        System.currentTimeMillis() + ".png", "drawing");

                if (imgSaved != null) {
                    Toast savedToast = Toast.makeText(getApplicationContext(),
                            "Painting saved to Gallery.", Toast.LENGTH_SHORT);
                    savedToast.show();
                }
                else {
                    Toast unsavedToast = Toast.makeText(getApplicationContext(),
                            "Painting failed to save.", Toast.LENGTH_SHORT);
                    unsavedToast.show();
                }
                canvasView.destroyDrawingCache();
            }
        });
        saveDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which){
                dialog.cancel();
            }
        });

        saveDialog.show();
    }



    //create a new canvas - display a dialog to confirm
    private void displayNewCanvasDialog() {
        AlertDialog.Builder newDialog = new AlertDialog.Builder(this);
        newDialog.setTitle("Create new painting?");
        newDialog.setMessage("Doing so will reset the current canvas and you will lose all progress.");
        newDialog.setPositiveButton("Create new painting", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which){
                canvasView.startNewCanvas();
                dialog.dismiss();
            }
        });
        newDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which){
                dialog.cancel();
            }
        });
        newDialog.show();
    }


    //undo -- todo
    public void undoLastDraw(View view) {
        canvasView.undoLastDrawRestoreToPreviousCanvas();
    }


    //send the painting to an email
    public void emailPainting(View view) {
       // email(getApplicationContext(), "fwwilcox@alaska.edu", "Painting", "New Painting", "");
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        //emailIntent.setType("application/image");
        emailIntent.setType("text/plain");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"fwwilcox@alaska.edu"});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"Test Subject");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "From My App");

        String imgLocation  = MediaStore.Images.Media.insertImage(
                getContentResolver(), canvasView.getDrawingCache(),
                System.currentTimeMillis() + ".png", "drawing");

        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(imgLocation));
        startActivity(Intent.createChooser(emailIntent, "Send email or text message"));
    }

    //take a screenshot
    public Bitmap takeScreenshot() {
        View rootView = findViewById(android.R.id.content).getRootView();
        rootView.setDrawingCacheEnabled(true);
        return rootView.getDrawingCache();
    }


    //set the brush color view a color picker
    public void setBrushColor(View view) {
            colorPicker.show();

            // On Click listener for the dialog, when the user select the color
            Button okColor = (Button)colorPicker.findViewById(R.id.okColorButton);
            okColor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                // You can get single channel (value 0-255)
                    int selectedColorR = colorPicker.getRed();
                    int selectedColorG = colorPicker.getGreen();
                    int selectedColorB = colorPicker.getBlue();

                    canvasView.setColor(selectedColorR, selectedColorG, selectedColorB);


                    Button colorButton = (Button) findViewById(R.id.colorButton);

                    colorButton.setBackgroundColor(colorPicker.getColor());
                    colorPicker.dismiss();

                }
            });
        }


    //take a photo of the canvas
    public void takePhoto (View view){
        startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), 3);
    }

    //set the canvas from a picture chosen from the androids camera roll/photos
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        //Detects request codes
        if(requestCode==3 && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);

               canvasView.setPicture(bitmap);

            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

}
