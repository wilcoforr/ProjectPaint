//Project Paint
//Main View for the Android App
//Sets up the canvas for drawing

package csce305.projectpaint;

import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PathEffect;
import android.view.View;

import android.content.Context;
import android.util.AttributeSet;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.MotionEvent;


import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.TypedValue;

import java.io.File;

public class CanvasView extends View {

    //private variables
    private Canvas canvas;

    private Bitmap canvasBitmap;

    private Path drawPath;

    private Canvas previousCanvas;
    private Path previousPath; //for undo feature WIP TODO

    private Paint drawPaint;
    private Paint canvasPaint;

    //default color is black
    private int paintColor = 0x000000;

    private float brushSize;
    private float previousBrushSize;

    private boolean isErasing = false;

    public CanvasView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupDrawing();
    }


    //methods
    
    
    //set up the canvas/view for drawing.
    private void setupDrawing() {
        drawPath = new Path();
        drawPaint = new Paint();

        brushSize = getResources().getInteger(R.integer.medium_size);
        previousBrushSize = brushSize;
        drawPaint.setStyle(Paint.Style.STROKE);

        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(brushSize);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
        canvasPaint = new Paint(Paint.DITHER_FLAG);
    }

    //set the brush to a round brush
    public void setBrushToRoundBrush() {
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
    }
    
    //set the brush to a square brush
    public void setBrushToSquareBrush() {
        drawPaint.setStrokeCap(Paint.Cap.SQUARE);
    }

    //create new event on sizeChanged (landscape to portrait?)
    @Override
    protected void onSizeChanged(int width, int height, int previousWidth, int previousHeight) {

        super.onSizeChanged(width, height, previousWidth, previousHeight);

        canvasBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        canvas = new Canvas(canvasBitmap);
    }


    //onDraw event
    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
        canvas.drawPath(drawPath, drawPaint);
    }

    public void drawCameraImageToCanvas(File imageFile) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();

       Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), bmOptions);
        //        canvasBitmap.isMutable();

        canvasBitmap = Bitmap.createScaledBitmap(bitmap,canvas.getWidth(),canvas.getHeight(),true);
        canvas.setBitmap(canvasBitmap);
        //        canvas = new Canvas(canvasBitmap);
       invalidate();
    }

    //on touch event - draw
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float touchX = event.getX();
        float touchY = event.getY();

        previousPath = drawPath;

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                drawPath.moveTo(touchX, touchY);
                break;
            case MotionEvent.ACTION_MOVE:
                drawPath.lineTo(touchX, touchY);
                break;
            case MotionEvent.ACTION_UP:
                canvas.drawPath(drawPath, drawPaint);
                drawPath.reset();
                break;
            default:
                return false;
        }

        invalidate(); //to trigger to redraw on view
        return true;
    }


    //set the chosen color to the brush
    public void setColor(String newColor){
        invalidate();
        paintColor = Color.parseColor(newColor);
        drawPaint.setColor(paintColor);
    }



    //set the brush's color from a selection of RGB values - via the color picker
    public void setColor(int red, int green, int blue){
        invalidate();
        paintColor = Color.rgb(red, green, blue);
        drawPaint.setColor(paintColor);
    }

    //set the size of the brush
    public void setBrushSize(float newSize){
        float pixelAmount = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, newSize, getResources().getDisplayMetrics());
        brushSize = pixelAmount;
        drawPaint.setStrokeWidth(brushSize);
    }

    //set the previous brush size
    public void setPreviousBrushSize(float lastSize){
        previousBrushSize = lastSize;
    }

    //get the previous brush size
    public float getPreviousBrushSize() {
        return previousBrushSize;
    }

    //set the erasing mode to true for erasing from the canvas
    public void setErase(boolean erase) {
        isErasing = erase;

        if(isErasing) {
            drawPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        }
        else {
            drawPaint.setXfermode(null);
        }
    }

    //Createa a new Canvas
    public void startNewCanvas(){
        canvas.drawColor(0, PorterDuff.Mode.CLEAR);
        invalidate();
    }

    //WIP TODO undo feature
    public void undoLastDrawRestoreToPreviousCanvas() {
        //canvas = previousCanvas;

        //canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
        invalidate();
    }

    //set a picture onto the canvas
    public void setPicture(Bitmap picture) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);

        canvas.drawBitmap(picture, 0, 0, paint);

    }


}
