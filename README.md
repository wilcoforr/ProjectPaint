# Project Paint #

Made: Fall 2016

An Android app that lets you draw on an image you can import from
your phone's gallery

Note: Code is NOT well documented since this was for an university class 
project (CSCE 305), completed during Fall semester 2016.

Thanks and credit to https://github.com/Pes8/android-material-color-picker-dialog
for the color picker dialog!


<img src="https://gitlab.com/wilcoforr/ProjectPaint/raw/master/Screenshot_20161203-134720.png" width="250" height="350" />

<img src="https://gitlab.com/wilcoforr/ProjectPaint/raw/master/Screenshot_20161203-134738.png"  width="250" height="350" />

<img src="https://gitlab.com/wilcoforr/ProjectPaint/raw/070ad05403c927443408178743b80514a83dd16b/Screenshot_20161203-135416.png"  width="250" height="350" />


